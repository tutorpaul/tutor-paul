# Contributing

Contributions will be accepted, after appropriate review, in the form of merge requests. Only changes pertaining to an open issue will be considered.

## Code quality

To ensure consistent formatting this project uses [black](https://black.readthedocs.io/en/stable/), [isort](https://timothycrosley.github.io/isort/), [flake8](https://flake8.pycqa.org/en/latest/), and [pytest](https://docs.pytest.org/en/latest/). There is config for the latter three tools within the codebase, changes to these config will require justification.

New features must have passing unittests. See the README for information on setting up a dev instance.

## Contributors

* [Paul Stiverson](https://gitlab.com/thismatters)
