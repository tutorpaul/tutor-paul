
build:
	@docker compose -p tp build
init:
	@docker compose -p tp up -d db
	@sleep 10
	@docker compose -p tp up -d app
	@docker compose -p tp exec app python manage.py createsuperuser
	@echo "App is ready to use. Use 'make stop' when you're done!"
stop:
	@docker compose -p tp down
run:
	@docker compose -p tp up -d
clean-database:
	@docker compose -p tp down -v
clean-images:
	@docker rmi `docker images -q -f "dangling=true"`
clean-cache:
	@docker rmi `docker images -a -q`
shell:
	@docker compose -p tp exec app sh
django-shell:
	@docker compose -p tp exec app ./manage.py shell
clean:
	@find . -name "__pycache__" | xargs rm -rfv
	@rm -rfv **/.hypothesis
	@docker system prune -f
	@echo "be sure to use sudo!"
migrations:
	@docker compose -p tp exec app python manage.py makemigrations
logs-app:
	@docker compose -p tp logs -f app
lint:
	docker compose -p tp exec app black .
	docker compose -p tp exec app isort .
	docker compose -p tp exec app pflake8
test:
	docker compose -p tp exec app pytest
