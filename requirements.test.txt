hypothesis==6.98.17
pytest-django==4.8.0
isort==5.13.2
black==24.2.0
pyproject-flake8==6.1.0
