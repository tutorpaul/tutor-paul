#!/bin/sh

if [[ $CONTAINER_PURPOSE == "app" ]]; then
    # do migration
    python manage.py migrate

    # start the app
    if [[ $DJANGO_SETTINGS_MODULE == "tutor_paul.settings.base" ]]; then
        python manage.py runserver 0.0.0.0:8000
    else
        gunicorn tutor_paul.wsgi:application -b 0.0.0.0:8000 --workers 4
    fi
elif [[ $CONTAINER_PURPOSE == "unittest" ]]; then
    # hold the container in a running state
    while $true; do sleep 60; done
fi
