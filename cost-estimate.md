# Operational Cost Estimate

## Domain Registration

## Hosting (AWS -- EB)

### Database -- RDS

* db.t3.micro $0.036 per Hour
* total $78.84 (annual if reserved for 3 years)

### App -- EB & EC2

* 1x t3.small 0.0208 per Hour (ea)
* total \~$182.20 (annual, on demand)
* total \~$70.08 (annual if reserved for 3 years)

### Static -- S3

* about 6 gigs of data stored
    * $0.023 per GB (monthly)
    * total $1.66
* 150-200 gb of transfer monthly
    * $0.09 per gb (1 free gig)
* total \~$60 (annual)

$193.92 (annual)


## Hosting (AWS -- Kubernetes)

Get some motherfucking AWS credits bruv!

### Database -- RDS

same as above (78.84/year)
* **ACTUAL** $224 (1 ea. 3 year res.) == $224

### App -- EKS & EC2

* 2x t3.medium $274-320 (annualy with 3 year reservation)
* **ACTUAL** $411 (2 ea. 3 year res.)  == $822


### Static -- S3

same as above (45/year)

\~$450 (annual)


## Hosting (Heroku)


### Dyno

$7 / month

### Data

$9 / month

$192 (annual)