from django.contrib import admin
from django.template.response import TemplateResponse
from django.urls import path

from solution.models import (
    Announcement,
    Comment,
    Concept,
    Course,
    Problem,
    Solution,
    Stream,
    StreamSupplement,
    Supplement,
    Textbook,
    TextbookEdition,
)


class SupplementInline(admin.TabularInline):
    model = Supplement
    extra = 1
    fields = ["name", "path"]


class StreamSupplementInline(admin.TabularInline):
    model = StreamSupplement
    extra = 1
    fields = ["name", "path"]


@admin.register(Concept)
class ConceptAdmin(admin.ModelAdmin):
    model = Concept


@admin.register(Solution)
class SolutionAdmin(admin.ModelAdmin):
    model = Solution
    inlines = (SupplementInline,)


@admin.register(Course)
class CourseAdmin(admin.ModelAdmin):
    model = Course


@admin.register(Stream)
class StreamAdmin(admin.ModelAdmin):
    model = Stream
    inlines = (StreamSupplementInline,)
    list_display = ("__str__", "course", "created")


@admin.register(Textbook)
class TextbookAdmin(admin.ModelAdmin):
    model = Textbook


@admin.register(TextbookEdition)
class TextbookEditionAdmin(admin.ModelAdmin):
    model = TextbookEdition


@admin.register(Problem)
class ProblemAdmin(admin.ModelAdmin):
    model = Problem
    list_display = (
        "__str__",
        "textbook_edition",
        "chapter_number",
        "problem_number",
        "method",
    )


def approve_comment(modeladmin, request, queryset):
    for comment in queryset:
        comment.user.commenter_status.approved = True
        comment.user.commenter_status.save()
        comment.needs_review = False
        comment.save()


approve_comment.short_description = "Approve Comment"


def reject_comment(modeladmin, request, queryset):
    for comment in queryset:
        comment.user.commenter_status.banned = True
        comment.user.commenter_status.save()
        comment.is_rejected = True
        comment.save()


reject_comment.short_description = "Reject Comment"


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    model = Comment
    list_display = (
        "user",
        "comment",
        "root_resource_type",
        "root_resource",
        "created",
        "needs_review",
        "is_rejected",
    )
    actions = [approve_comment, reject_comment]

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [
            path(
                "<int:pk>/approve/",
                self.admin_site.admin_view(self.approve_comment),
                name="approve_comment",
            ),
            path(
                "<int:pk>/reject/",
                self.admin_site.admin_view(self.reject_comment),
                name="reject_comment",
            ),
        ]
        return my_urls + urls

    def approve_comment(self, request, pk):
        # it might be good to automatically approve any comment by the user...
        comment = Comment.objects.get(pk=pk)
        comment.user.commenter_status.approved = True
        comment.user.commenter_status.save()
        comment.needs_review = False
        comment.save()
        context = {"action": "approved", "comment": comment}
        return TemplateResponse(request, "comment_action.html", context)

    def reject_comment(self, request, pk):
        # it might be good to automatically approve any comment by the user...
        comment = Comment.objects.get(pk=pk)
        comment.user.commenter_status.banned = True
        comment.user.commenter_status.save()
        comment.is_rejected = True
        comment.save()
        context = {"action": "rejected", "comment": comment}
        return TemplateResponse(request, "comment_action.html", context)

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False


@admin.register(Announcement)
class AnnouncementAdmin(admin.ModelAdmin):
    model = Announcement
    list_display = (
        "pk",
        "course",
        "text",
        "show_date",
        "remove_date",
    )
