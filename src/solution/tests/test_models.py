from django.test import TestCase
from hypothesis import given
from hypothesis import strategies as st
from hypothesis.extra.django import TestCase as HypothesisTestCase
from hypothesis.extra.django import from_model

from solution import models as m
from solution.tests.base import ModelInstanceMixin


class HypothesisModelTestCase(HypothesisTestCase, ModelInstanceMixin):
    def _base_model_test(self, *, model, strategy=None):
        if strategy is None:
            strategy = from_model(model)

        @given(strategy)
        def _test(instance):
            self.assertIsInstance(instance, model)
            self.assertIsNotNone(instance.pk)

        _test()

    def test_concept_model(self):
        self._base_model_test(model=m.Concept)

    def test_solution_model(self):
        self._base_model_test(model=m.Solution)

    def test_supplement_model(self):
        strategy = from_model(m.Supplement, solution=st.just(self._solution()))
        self._base_model_test(model=m.Supplement, strategy=strategy)

    def test_textbook_model(self):
        self._base_model_test(model=m.Textbook)

    def test_textbook_edition(self):
        strategy = from_model(m.TextbookEdition, textbook=st.just(self._textbook()))
        self._base_model_test(model=m.TextbookEdition, strategy=strategy)

    def test_textbook_edition_preference(self):
        """This model is only foreign keys, no field tests necessary"""
        pass

    def test_problem_model(self):
        strategy = from_model(
            m.Problem,
            textbook_edition=st.just(self._textbook_edition()),
            solution=st.just(self._solution()),
        )
        self._base_model_test(model=m.Problem, strategy=strategy)

    def test_comment_model(self):
        strategy = from_model(
            m.Comment,
            user=st.just(self._user()),
            solution=st.just(self._solution()),
            concept=st.just(None),
            in_response_to=st.just(None),
        )
        self._base_model_test(model=m.Comment, strategy=strategy)


class CommentStatusTestCase(TestCase, ModelInstanceMixin):
    def setUp(self):
        self.user = self._user()
        super().setUp()

    def test_auto_rejected_comment(self):
        # set user status to banned
        self._commenter_status(user=self.user, banned=True)
        # create comment for user
        comment = self._comment(user=self.user, is_rejected=False)
        # verify that it is_rejected
        self.assertTrue(comment.is_rejected)

    def test_unapproved_comment(self):
        # set user status approved = False
        self._commenter_status(user=self.user, approved=False)
        # create a comment for user
        comment = self._comment(user=self.user, needs_review=False)
        # verify that comment is needs_review
        self.assertTrue(comment.needs_review)

    def test_approved_comment(self):
        self._commenter_status(user=self.user, approved=True, banned=False)
        comment = self._comment(user=self.user, needs_review=True, is_rejected=True)
        self.assertFalse(comment.needs_review)
        self.assertFalse(comment.is_rejected)
