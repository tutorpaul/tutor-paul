from unittest.mock import patch

from django.test import RequestFactory, TestCase, override_settings
from django.urls import reverse

from solution import views
from solution.tests.base import ModelInstanceMixin


def setup_view(view, request, *args, **kwargs):
    """Mimic as_view() returned callable, but returns view instance.

    args and kwargs are the same you would pass to ``reverse()``

    """
    view.request = request
    view.args = args
    view.kwargs = kwargs
    return view


class ViewTestCase(TestCase, ModelInstanceMixin):
    def setUp(self):
        self.user = self._user(username="testuser", password="pass0209")
        # Setup request and view.
        self.request = RequestFactory().get("/fake-path")
        self.request.user = self.user

    def test_textbook_view_get_context_data(self):
        book1 = self._textbook(name="some boring book", slug="boring-book")
        book1_e1 = self._textbook_edition(textbook=book1, edition="1", slug="boring-1")
        book1_e2 = self._textbook_edition(textbook=book1, edition="2", slug="boring-2")
        book1_e3 = self._textbook_edition(textbook=book1, edition="3", slug="boring-3")
        book2 = self._textbook(name="some interesting book", slug="interesting-book")
        book2_e1 = self._textbook_edition(
            textbook=book2, edition="1", slug="interesting-1"
        )
        book2_e2 = self._textbook_edition(
            textbook=book2, edition="2", slug="interesting-2"
        )
        book2_e3 = self._textbook_edition(
            textbook=book2, edition="3", slug="interesting-3"
        )
        book3 = self._textbook(name="some interesting book", slug="other-book")
        book3_e1 = self._textbook_edition(textbook=book3, edition="1", slug="other-1")
        book3_e2 = self._textbook_edition(textbook=book3, edition="2", slug="other-2")
        view = setup_view(views.TextbookView(), self.request)

        view.object_list = view.get_queryset()
        context = view.get_context_data()
        self.assertEqual(
            [
                {"textbook": book1, "editions": [book1_e1, book1_e2, book1_e3]},
                {"textbook": book2, "editions": [book2_e1, book2_e2, book2_e3]},
                {"textbook": book3, "editions": [book3_e1, book3_e2]},
            ],
            context["object_list"],
        )

    @patch.object(views.ProblemIndexView, "get")
    def test_problem_index_view_dispatch(self, p_get):
        """Verify that the user textbook preference is set"""
        solution = self._solution()
        book = self._textbook()
        edition1 = self._textbook_edition(textbook=book, edition="1", slug="edition-1")
        edition2 = self._textbook_edition(textbook=book, edition="2", slug="edition-2")
        view = setup_view(
            views.ProblemIndexView(), self.request, textbook_edition_slug="edition-1"
        )
        view.dispatch(self.request, textbook_edition_slug="edition-1")
        # check the users textbook preference
        prefs = self.user.edition_preferences.all()
        self.assertEqual(len(prefs), 1)
        self.assertEqual(prefs[0].textbook_edition, edition1)
        view = setup_view(
            views.ProblemIndexView(), self.request, textbook_edition_slug="edition-2"
        )
        view.dispatch(self.request, textbook_edition_slug="edition-2")
        # check the users textbook preference
        prefs = self.user.edition_preferences.all()
        self.assertEqual(len(prefs), 1)
        self.assertEqual(prefs[0].textbook_edition, edition2)

    def test_problem_index_view_get_queryset(self):
        book = self._textbook()
        edition1 = self._textbook_edition(textbook=book, edition="1", slug="edition-1")
        edition2 = self._textbook_edition(textbook=book, edition="2", slug="edition-2")
        edition1_problems = []
        edition2_problems = []
        for chapter_number in range(4):
            for problem_number in range(10):
                edition1_problems.append(
                    self._problem(
                        textbook_edition=edition1,
                        problem_number=problem_number,
                        chapter_number=chapter_number,
                    )
                )
                edition2_problems.append(
                    self._problem(
                        textbook_edition=edition2,
                        problem_number=problem_number,
                        chapter_number=chapter_number,
                    )
                )
                e1_c1_p1 = self._problem(
                    textbook_edition=edition1, problem_number="2", chapter_number="1"
                )
                e1_c1_p1 = self._problem(
                    textbook_edition=edition1, problem_number="3", chapter_number="1"
                )
        view = setup_view(
            views.ProblemIndexView(), self.request, textbook_edition_slug="edition-1"
        )
        qs = view.get_queryset()
        self.assertEqual(edition1_problems, list(qs))
        view = setup_view(
            views.ProblemIndexView(), self.request, textbook_edition_slug="edition-2"
        )
        qs = view.get_queryset()
        self.assertEqual(edition2_problems, list(qs))

    def test_problem_view_get_redirect_url_good(self):
        edition1 = self._textbook_edition(edition="1", slug="edition-1")
        solution = self._solution(slug="this-one-please")
        self._problem(
            textbook_edition=edition1,
            problem_number="1",
            chapter_number="1",
            solution=solution,
        )
        view = setup_view(
            views.ProblemView(),
            self.request,
            textbook_edition_slug="edition-1",
            chapter_number=1,
            problem_number=1,
        )
        url = view.get_redirect_url(
            textbook_edition_slug="edition-1",
            chapter_number=1,
            problem_number=1,
        )
        self.assertEqual(
            url,
            reverse("solutions:solution", kwargs={"solution_slug": "this-one-please"}),
        )

    @patch("solution.views.messages.error")
    def test_problem_view_get_redirect_url_bad(self, p_message_error):
        edition1 = self._textbook_edition(edition="1", slug="edition-1")
        self._problem(textbook_edition=edition1, problem_number="1", chapter_number="1")
        view = setup_view(
            views.ProblemView(),
            self.request,
            textbook_edition_slug="edition-1",
            chapter_number=1,
            problem_number=2,
        )
        url = view.get_redirect_url(
            textbook_edition_slug="edition-1",
            chapter_number=1,
            problem_number=2,
        )
        self.assertEqual(
            url,
            reverse(
                "solutions:problem_index", kwargs={"textbook_edition_slug": "edition-1"}
            ),
        )
        p_message_error.assert_called_once()

    def test_solution_view_get_context_data(self):
        # create test solution
        solution = self._solution(slug="test-solution")
        problem = self._problem(solution=solution)
        self._textbook_edition_preference(
            user=self.user, textbook_edition=problem.textbook_edition
        )
        view = setup_view(
            views.SolutionView(), self.request, solution_slug="test-solution"
        )
        view.object = view.get_object()
        context = view.get_context_data()
        self.assertEqual(context["element_type"], "solution")
        self.assertEqual(context["element"], solution)
        self.assertEqual(context["object"], solution)
        self.assertEqual(context["current"], problem)

    def test_concept_view_get_context_data(self):
        concept = self._concept(slug="test-concept")
        self._concept(slug="other-concept")
        view = setup_view(
            views.ConceptView(), self.request, concept_slug="test-concept"
        )
        view.object = view.get_object()
        context = view.get_context_data()
        self.assertEqual(context["element"], concept)
        self.assertEqual(context["element_type"], "concept")
