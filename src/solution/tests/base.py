from random import randint

from django.contrib.auth import get_user_model

from solution import models as m


class ModelInstanceMixin:
    def _concept(self, **kwargs):
        default_kwargs = {
            "name": "test concept",
            "slug": "test-concept",
            "video": "test_video.mp4",
            "opening_card": "test_image.png",
        }
        default_kwargs.update(kwargs)
        concept, _ = m.Concept.objects.get_or_create(**default_kwargs)
        return concept

    def _solution(self, **kwargs):
        concepts = kwargs.pop("concepts", [])
        default_kwargs = {
            "name": "test concept",
            "slug": "test-concept",
            "video": "video/test_video.mp4",
            "opening_card": "cards/test_image.png",
        }
        default_kwargs.update(kwargs)
        solution, _ = m.Solution.objects.get_or_create(**default_kwargs)
        for concept in concepts:
            solution.concepts.add(concept)
        return solution

    def _supplement(self, **kwargs):
        solution = kwargs.pop("solution", None)
        if solution is None:
            solution = self._solution()
        default_kwargs = {
            "solution": solution,
            "name": "test supplement",
            "path": "supplements/test_supplement.png",
        }
        default_kwargs.update(kwargs)
        supplement, _ = m.Supplement.objects.get_or_create(**default_kwargs)
        return supplement

    def _textbook(self, **kwargs):
        default_kwargs = {
            "author": "test author",
            "name": "test textbook",
            "slug": "test-textbook",
        }
        default_kwargs.update(kwargs)
        textbook, _ = m.Textbook.objects.get_or_create(**default_kwargs)
        return textbook

    def _textbook_edition(self, **kwargs):
        textbook = kwargs.pop("textbook", None)
        if textbook is None:
            textbook = self._textbook()
        default_kwargs = {
            "textbook": textbook,
            "edition": "ultimate edition",
            "slug": "text-textbook-edition",
        }
        default_kwargs.update(kwargs)
        textbook_edition, _ = m.TextbookEdition.objects.get_or_create(**default_kwargs)
        return textbook_edition

    def _textbook_edition_preference(self, **kwargs):
        user = kwargs.pop("user", None)
        if user is None:
            user = self._user()
        textbook_edition = kwargs.pop("textbook_edition", None)
        if textbook_edition is None:
            textbook_edition = self._textbook_edition()
        default_kwargs = {"user": user, "textbook_edition": textbook_edition}
        default_kwargs.update(kwargs)
        (
            textbook_edition_preference,
            _,
        ) = m.TextbookEditionPreference.objects.get_or_create(**default_kwargs)
        return textbook_edition_preference

    def _problem(self, **kwargs):
        textbook_edition = kwargs.pop("textbook_edition", None)
        if textbook_edition is None:
            textbook_edition = self._textbook_edition()
        solution = kwargs.pop("solution", None)
        if solution is None:
            solution = self._solution()
        default_kwargs = {
            "textbook_edition": textbook_edition,
            "solution": solution,
            "chapter_number": 1,
            "problem_number": 12,
        }
        default_kwargs.update(kwargs)
        problem, _ = m.Problem.objects.get_or_create(**default_kwargs)
        return problem

    def _comment(self, **kwargs):
        user = kwargs.pop("user", None)
        if user is None:
            user = self._user()
        solution = kwargs.pop("solution", None)
        concept = kwargs.pop("concept", None)
        in_response_to = kwargs.pop("in_response_to", None)
        if solution is None and concept is None and in_response_to is None:
            solution = self._solution()
        default_kwargs = {
            "user": user,
            "solution": solution,
            "concept": concept,
            "in_response_to": in_response_to,
            "comment": "Just a test comment",
        }
        default_kwargs.update(kwargs)
        comment, _ = m.Comment.objects.get_or_create(**default_kwargs)
        return comment

    def _commenter_status(self, *, user, **kwargs):
        commenter_status, _ = m.CommenterStatus.objects.get_or_create(
            user=user, **kwargs
        )
        return commenter_status

    def _user(self, username=None, password="testuser12345", email="test@user.com"):
        if username is None:
            username = "testuser{}".format(randint(0, 9999))
        _user_model = get_user_model()
        try:
            user = _user_model.objects.get(username=username)
        except _user_model.DoesNotExist:
            user = _user_model.objects.create_user(
                username=username,
                email=email,
                password=password,
            )
        return user
