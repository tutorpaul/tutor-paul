"""tutor_paul URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path

from solution import views

app_name = "solutions"

urlpatterns = [
    # all textbook (versions)
    path("textbooks/", views.TextbookView.as_view(), name="textbooks"),
    # all problems in textbook
    path(
        "textbooks/<slug:textbook_edition_slug>/",
        views.ProblemIndexView.as_view(),
        name="problem_index",
    ),
    # specific problem
    path(
        "textbooks/<slug:textbook_edition_slug>/<int:chapter_number>-<int:problem_number>/",
        views.ProblemView.as_view(),
        name="problem",
    ),
    path("concepts/", views.ConceptIndexView.as_view(), name="concepts"),
    path("concepts/<slug:concept_slug>/", views.ConceptView.as_view(), name="concept"),
    path("streams/", views.StreamIndexView.as_view(), name="streams"),
    path(
        "streams/course/<slug:course_slug>/",
        views.StreamByCourseView.as_view(),
        name="course_streams",
    ),
    path("streams/<slug:stream_slug>/", views.StreamView.as_view(), name="stream"),
    path("<slug:solution_slug>/", views.SolutionView.as_view(), name="solution"),
]
