from django.core.management.base import BaseCommand

from solution.models import (
    Concept,
    Problem,
    Solution,
    Textbook,
    TextbookEdition,
)

# from django.core.files import File as DjangoFile


class Command(BaseCommand):
    help = "Creates some stuff for demo/testing"

    def handle(self, *args, **options):
        childs, _ = Textbook.objects.get_or_create(
            author="Childs, Dara", name="Dynamics and Vibrations", slug="childs"
        )
        childs10, _ = TextbookEdition.objects.get_or_create(
            textbook=childs, edition="10", slug="childs-10"
        )
        childs11, _ = TextbookEdition.objects.get_or_create(
            textbook=childs, edition="11", slug="childs-11"
        )

        dcm, _ = Concept.objects.get_or_create(
            name="Directon Cosine Matrix",
            slug="dcm",
            video="some/path/to/video.mp4",
            opening_card="some/path/to/card.png",
            worksheet="some/path/to/worksheet.png",
        )

        s1_12, _ = Solution.objects.get_or_create(
            name="Leaving on a jetplane",
            slug="leaving-on-a-jetplane",
            video="some/other/path/video.mp4",
            opening_card="some/other/path/card.png",
            worksheet="some/other/path/worksheet.png",
        )
        s1_12.concepts.add(dcm)

        s1_14, _ = Solution.objects.get_or_create(
            name="Roller Coaster",
            slug="roller-coaster",
            video="coaster/path/video.mp4",
            opening_card="coaster/path/card.png",
            worksheet="coaster/path/worksheet.png",
        )
        s1_14.concepts.add(dcm)

        Problem.objects.get_or_create(
            textbook_edition=childs10,
            chapter_number=1,
            problem_number=12,
            solution=s1_12,
        )
        Problem.objects.get_or_create(
            textbook_edition=childs10,
            chapter_number=1,
            problem_number=14,
            solution=s1_14,
        )
        Problem.objects.get_or_create(
            textbook_edition=childs11,
            chapter_number=1,
            problem_number=15,
            solution=s1_12,
        )
        Problem.objects.get_or_create(
            textbook_edition=childs11,
            chapter_number=1,
            problem_number=13,
            solution=s1_14,
        )
