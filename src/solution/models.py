import os

import boto3
from django.conf import settings
from django.core.mail import send_mail
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.urls import reverse
from django.utils import timezone
from django.utils.functional import cached_property

s3 = boto3.client(
    "s3",
    region_name=settings.AWS_S3_REGION_NAME,
    aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
    aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
    endpoint_url=settings.AWS_S3_ENDPOINT_URL,
)


def _generate_presigned_url(key):
    key = os.path.join(settings.AWS_LOCATION, key)
    presigned_url = s3.generate_presigned_url(
        ClientMethod="get_object",
        Params={
            "Bucket": settings.AWS_STORAGE_BUCKET_NAME,
            "Key": key,
        },
        ExpiresIn=28800,
    )
    return presigned_url


class Course(models.Model):
    name = models.CharField(max_length=64)
    slug = models.SlugField(max_length=64, unique=True)

    def __str__(self):
        return self.name


class Resource(models.Model):
    """References the actual files for display"""

    name = models.CharField(max_length=64)
    slug = models.SlugField(max_length=64, unique=True)
    # should these be filefields or filepathfields?
    # since they will be living on the filesystem prior to app creation
    #   they might need to be a CharField
    # No, I think they should be an actual filefield to more seamlessly
    #   deal with file retrieval. The files can be created in place by
    #   django, seeding from a zip file or from the prior static location
    video = models.CharField(max_length=128)
    opening_card = models.CharField(max_length=128)
    course = models.ForeignKey(
        Course, related_name="%(model_name)ss", on_delete=models.PROTECT, null=True
    )

    class Meta:
        abstract = True

    def __str__(self):
        return self.name

    @property
    def video_url(self):
        return _generate_presigned_url(self.video)

    @property
    def opening_card_url(self):
        return _generate_presigned_url(self.opening_card)


class Concept(Resource):
    pass


class Solution(Resource):
    concepts = models.ManyToManyField(Concept, related_name="solutions")


class BaseSupplement(models.Model):
    name = models.CharField(max_length=64, blank=True, null=True, default=None)
    path = models.CharField(max_length=128)

    class Meta:
        abstract = True

    @property
    def url(self):
        return _generate_presigned_url(self.path)


class Supplement(BaseSupplement):
    solution = models.ForeignKey(
        Solution, on_delete=models.CASCADE, related_name="supplements"
    )


class Stream(Resource):
    created = models.DateField(null=True)


class StreamSupplement(BaseSupplement):
    stream_archive = models.ForeignKey(
        Stream, on_delete=models.CASCADE, related_name="supplements"
    )


class Textbook(models.Model):
    author = models.CharField(max_length=256)
    name = models.CharField(max_length=128)
    slug = models.SlugField(max_length=128, unique=True)

    def __str__(self):
        return self.name


class TextbookEdition(models.Model):
    textbook = models.ForeignKey(
        Textbook, on_delete=models.CASCADE, related_name="editions"
    )
    edition = models.CharField(max_length=64)
    slug = models.SlugField(max_length=136, unique=True)

    def __str__(self):
        return "{} {} Edition".format(self.textbook, self.edition)

    def set_as_preferred_for_user(self, user):
        try:
            current_selection = user.edition_preferences.get(
                textbook_edition__textbook=self.textbook
            )
        except TextbookEditionPreference.DoesNotExist:
            current_selection = TextbookEditionPreference.objects.create(
                user=user, textbook_edition=self
            )
        else:
            if current_selection.textbook_edition != self:
                current_selection.textbook_edition = self
                current_selection.save()


class TextbookEditionPreference(models.Model):
    """Store which version of the textbook a user uses."""

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="edition_preferences",
    )
    textbook_edition = models.ForeignKey(TextbookEdition, on_delete=models.CASCADE)

    class Meta:
        constraints = [models.UniqueConstraint(fields=("user",), name="user_textbook")]


class Problem(models.Model):
    textbook_edition = models.ForeignKey(
        TextbookEdition, on_delete=models.CASCADE, related_name="problems"
    )
    chapter_number = models.IntegerField()
    problem_number = models.IntegerField()
    method = models.CharField(max_length=32, null=True, blank=True)
    solution = models.ForeignKey(Solution, on_delete=models.PROTECT)

    class Meta:
        ordering = ["chapter_number", "problem_number"]


class CommenterStatus(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="commenter_status",
    )
    approved = models.BooleanField(null=True, default=False)
    banned = models.BooleanField(null=True, default=False)


class Comment(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT)
    solution = models.ForeignKey(
        Solution, on_delete=models.CASCADE, null=True, blank=True, default=None
    )
    concept = models.ForeignKey(
        Concept, on_delete=models.CASCADE, null=True, blank=True, default=None
    )
    stream = models.ForeignKey(
        Stream, on_delete=models.CASCADE, null=True, blank=True, default=None
    )
    comment = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    needs_review = models.BooleanField(
        null=True,
        default=True,
        help_text=(
            "An un-reviewed comment will be displayed to the user who "
            "created it and be marked as awaiting review"
        ),
    )
    is_rejected = models.BooleanField(
        null=True,
        default=False,
        help_text="A rejected comment will only appear for the user who created it",
    )
    in_response_to = models.ForeignKey(
        "self",
        on_delete=models.CASCADE,
        related_name="replies",
        null=True,
        blank=True,
        default=None,
    )

    @cached_property
    def root_resource(self):
        return (
            self.concept
            or self.solution
            or self.in_response_to.root_resource
            or self.stream
        )

    def root_resource_type(self):
        resource_type = "solution"
        if isinstance(self.root_resource, Concept):
            resource_type = "concept"
        if isinstance(self.root_resource, Stream):
            resource_type = "stream"
        return resource_type

    def save(self, *args, **kwargs):
        if not hasattr(self.user, "commenter_status"):
            print("creating CommenterStatus")
            CommenterStatus.objects.get_or_create(user=self.user)
        self.needs_review = not self.user.commenter_status.approved
        self.is_rejected = self.user.commenter_status.banned
        super().save(*args, **kwargs)


class Announcement(models.Model):
    course = models.ForeignKey(
        Course, related_name="announcements", on_delete=models.PROTECT
    )
    text = models.CharField(max_length=256)
    show_date = models.DateTimeField(default=timezone.now)
    remove_date = models.DateTimeField()


# post_save hook to send new comment email
@receiver(post_save, sender=Comment)
def notify_comment(sender, instance, created, **kwargs):
    if not created:
        # don't notify me about edits... this could be abused
        return
    if instance.is_rejected:
        # don't spam me with this shit
        return
    resource = instance.root_resource
    comment_type = "comment"
    if instance.in_response_to is not None:
        comment_type = "reply"

    resource_type = instance.root_resource_type()
    urls = {
        "approve_url": reverse("admin:approve_comment", kwargs={"pk": instance.pk}),
        "reject_url": reverse("admin:reject_comment", kwargs={"pk": instance.pk}),
        "delete_url": reverse("admin:solution_comment_delete", args=(instance.pk,)),
        "resource_url": reverse(
            "solutions:{}".format(resource_type),
            kwargs={"{}_slug".format(resource_type): resource.slug},
        ),
    }
    subject = "New {} by {} on {} {}".format(
        comment_type, instance.user, resource_type, resource
    )
    review_text = "!!! awaiting review !!! " if instance.needs_review else ""
    message = """
{review_text}Comment reads: ```
{comment}
```
Actions:
* Approve: {base_url}{approve_url}
* Reject: {base_url}{reject_url}
* Delete: {base_url}{delete_url}
* View: {base_url}{resource_url}

""".format(
        base_url=settings.BASE_URL,
        comment=instance.comment,
        review_text=review_text,
        **urls
    )
    send_mail(
        subject,
        message,
        settings.CONTACT_SENDER,
        [settings.CONTACT_EMAIL],
    )
