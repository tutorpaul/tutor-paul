# Generated by Django 2.2.5 on 2021-09-19 11:46

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("solution", "0011_remove_stream_created"),
    ]

    operations = [
        migrations.RenameField(
            model_name="stream",
            old_name="create_date",
            new_name="created",
        ),
    ]
