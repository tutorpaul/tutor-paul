# Generated by Django 2.2.5 on 2021-09-19 12:47

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("solution", "0012_auto_20210919_1146"),
    ]

    operations = [
        migrations.AddField(
            model_name="comment",
            name="stream",
            field=models.ForeignKey(
                blank=True,
                default=None,
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                to="solution.Stream",
            ),
        ),
    ]
