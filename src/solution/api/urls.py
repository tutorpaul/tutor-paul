from rest_framework import routers

from solution.api.views import (
    CommentViewSet,
    CourseViewSet,
    StreamSupplementViewSet,
    StreamViewSet,
)

# from django.urls import path


app_name = "solutions-api"

router = routers.SimpleRouter()
router.register(r"comments", CommentViewSet, basename="comment")
router.register(r"courses", CourseViewSet, basename="course")
router.register(r"streams", StreamViewSet, basename="stream")
router.register(
    r"streamSupplements", StreamSupplementViewSet, basename="stream_supplement"
)
urlpatterns = router.urls
