import bleach
from django.contrib.auth import get_user_model
from rest_framework.serializers import (
    ModelSerializer,
    PrimaryKeyRelatedField,
    SlugRelatedField,
    StringRelatedField,
    ValidationError,
)
from rest_framework_recursive.fields import RecursiveField

from solution.models import (
    Comment,
    Concept,
    Course,
    Solution,
    Stream,
    StreamSupplement,
)


class CourseSerializer(ModelSerializer):
    class Meta:
        model = Course
        fields = ("name", "slug", "pk")
        extra_kwargs = {"pk": {"read_only": True}}


class StreamSerializer(ModelSerializer):
    course_slug = SlugRelatedField(
        slug_field="slug", queryset=Course.objects.all(), source="course"
    )

    class Meta:
        model = Stream
        fields = (
            "pk",
            "slug",
            "course_slug",
            "created",
            "name",
            "video",
            "opening_card",
        )
        extra_kwargs = {"pk": {"read_only": True}}


class StreamSupplementSerializer(ModelSerializer):
    stream_pk = PrimaryKeyRelatedField(
        queryset=Stream.objects.all(), source="stream_archive"
    )

    class Meta:
        model = StreamSupplement
        fields = ("pk", "stream_pk", "name", "path")
        extra_kwargs = {"pk": {"read_only": True}}


class CommentSerializer(ModelSerializer):
    replies = RecursiveField(required=False, allow_null=True, many=True)
    user_pk = PrimaryKeyRelatedField(
        queryset=get_user_model().objects.all(), source="user", required=False
    )
    user_name = StringRelatedField(source="user")
    solution_pk = PrimaryKeyRelatedField(
        queryset=Solution.objects.all(), source="solution", required=False
    )
    concept_pk = PrimaryKeyRelatedField(
        queryset=Concept.objects.all(), source="concept", required=False
    )
    in_response_to_pk = PrimaryKeyRelatedField(
        queryset=Comment.objects.all(), source="in_response_to", required=False
    )

    class Meta:
        model = Comment
        fields = (
            "pk",
            "user_pk",
            "user_name",
            "created",
            "comment",
            "needs_review",
            "replies",
            "solution_pk",
            "concept_pk",
            "in_response_to_pk",
        )
        extra_kwargs = {"pk": {"read_only": True}}
        read_only_fields = ("needs_review",)

    def validate_comment(self, value):
        return bleach.clean(value)

    def validate(self, attrs):
        if self.instance:
            print(self.context["request"].user)
            # print(vars(self))
            return attrs
        # ensure that there is one of:
        #    solution_pk, concept_pk, in_response_to_pk
        one_of_these = ["solution", "concept", "in_response_to"]
        one_of_these_pk = ["solution_pk", "concept_pk", "in_response_to_pk"]
        values = {}
        for this in one_of_these:
            values[this] = attrs.get(this)

        if not any([v for v in values.values()]):
            raise ValidationError(
                "Comment must be created with one of these: " + ", ".join(one_of_these)
            )
        for this, value in values.items():
            others = list(one_of_these)
            others.remove(this)
            if value and any([values[other] for other in others]):
                raise ValidationError(
                    "Comment can only be created with *one* of these: "
                    + ", ".join(one_of_these_pk)
                )
        return attrs
