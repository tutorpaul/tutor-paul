from datetime import datetime, timedelta

from django.contrib.auth import get_user_model
from django.urls import reverse
from django.utils.timezone import make_aware
from hypothesis import assume, given
from hypothesis import strategies as st
from hypothesis.extra.django import TestCase
from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from solution.models import Comment, CommenterStatus, Concept, Solution
from solution.tests.base import ModelInstanceMixin


class BaseCommentAPITestCase(APITestCase):
    def setUp(self):
        self.main_user = get_user_model().objects.create_user(
            username="maintestuser", password="pass0209"
        )
        self.other_user = get_user_model().objects.create_user(
            username="othertestuser", password="pass0209"
        )
        self.client.login(username="maintestuser", password="pass0209")


class SolutionCommentAPITestCase(BaseCommentAPITestCase):
    def setUp(self):
        super().setUp()
        self.solution = Solution.objects.create(
            name="test solution",
            slug="test-solution",
            video="asfasdf",
            opening_card="aasfasdf",
        )

    def test_post_comment(self):
        url = reverse("solutions-api:comment-list")
        response = self.client.post(
            url,
            {
                "solution_pk": self.solution.pk,
                "comment": "This is just a test comment!",
            },
        )
        print(response.json())
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        comments = Comment.objects.filter(solution=self.solution)
        self.assertEqual(len(comments), 1)

    def test_get_comment(self):
        # create a comment
        Comment.objects.create(
            solution=self.solution, user=self.main_user, comment="some comment"
        )
        # retrieve it from api
        url = reverse("solutions-api:comment-list")
        response = self.client.get(url)
        # confirm a match
        self.assertContains(response, "some comment")

    def test_get_comments_by_slug(self):
        other_solution = Solution.objects.create(
            name="other solution",
            slug="other-solution",
            video="asfasdf",
            opening_card="aasfasdf",
        )
        Comment.objects.create(
            solution=self.solution, user=self.main_user, comment="some comment"
        )
        Comment.objects.create(
            solution=other_solution, user=self.main_user, comment="another comment"
        )
        url = reverse("solutions-api:comment-list") + "?solution_slug=test-solution"
        response = self.client.get(url)
        # confirm a match
        self.assertEqual(len(response.json()), 1)
        self.assertContains(response, "some comment")

        url = reverse("solutions-api:comment-list") + "?solution_slug=other-solution"
        response = self.client.get(url)
        # confirm a match
        self.assertEqual(len(response.json()), 1)
        self.assertContains(response, "another comment")


class ConceptCommentAPITestCase(BaseCommentAPITestCase):
    def setUp(self):
        super().setUp()
        self.concept = Concept.objects.create(
            name="test concept",
            slug="test-concept",
            video="asfasdf",
            opening_card="aasfasdf",
        )

    def test_post_comment(self):
        url = reverse("solutions-api:comment-list")
        response = self.client.post(
            url,
            {
                "concept_pk": self.concept.pk,
                "comment": "This is just a test comment!",
            },
        )
        print(response.json())
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        comments = Comment.objects.filter(concept=self.concept)
        self.assertEqual(len(comments), 1)

    def test_get_comment(self):
        # create a comment
        Comment.objects.create(
            concept=self.concept, user=self.main_user, comment="some comment"
        )
        # retrieve it from api
        url = reverse("solutions-api:comment-list")
        response = self.client.get(url)
        # confirm a match
        self.assertContains(response, "some comment")

    def test_get_comments_by_slug(self):
        other_concept = Concept.objects.create(
            name="other concept",
            slug="other-concept",
            video="asfasdf",
            opening_card="aasfasdf",
        )
        Comment.objects.create(
            concept=self.concept, user=self.main_user, comment="some comment"
        )
        Comment.objects.create(
            concept=other_concept, user=self.main_user, comment="another comment"
        )
        url = reverse("solutions-api:comment-list") + "?concept_slug=test-concept"
        response = self.client.get(url)
        # confirm a match
        self.assertEqual(len(response.json()), 1)
        self.assertContains(response, "some comment")

        url = reverse("solutions-api:comment-list") + "?concept_slug=other-concept"
        response = self.client.get(url)
        # confirm a match
        self.assertEqual(len(response.json()), 1)
        self.assertContains(response, "another comment")


class CommentReplyAPITestCase(BaseCommentAPITestCase):
    def setUp(self):
        super().setUp()
        self.solution = Solution.objects.create(
            name="test solution",
            slug="test-solution",
            video="asfasdf",
            opening_card="aasfasdf",
        )
        self.comment = Comment.objects.create(
            user=self.main_user,
            solution=self.solution,
            comment="original comment",
        )

    def test_post_comment(self):
        """Post a reply to a comment"""
        url = reverse("solutions-api:comment-list")
        response = self.client.post(
            url,
            {
                "in_response_to_pk": self.comment.pk,
                "comment": "This is just a test reply!",
            },
        )
        print(response.json())
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        comments = Comment.objects.filter(in_response_to=self.comment)
        self.assertEqual(len(comments), 1)

    def test_get_comment(self):
        """Ensure that only top-level comments are presented"""
        reply = self.comment
        for i in range(5):
            reply = Comment.objects.create(
                user=self.main_user,
                in_response_to=reply,
                comment="{}th reply".format(i),
            )

        url = reverse("solutions-api:comment-list")
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()), 1)

    def test_get_comment_with_replies(self):
        reply = self.comment
        for i in range(5):
            reply = Comment.objects.create(
                user=self.main_user,
                in_response_to=reply,
                comment="{}th reply".format(i),
            )
        url = reverse("solutions-api:comment-list")
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, "4th reply")


class BadCommentAPITestCase(BaseCommentAPITestCase):
    def setUp(self):
        super().setUp()
        self.solution = Solution.objects.create(
            name="test solution",
            slug="test-solution",
            video="asfasdf",
            opening_card="aasfasdf",
        )
        self.concept = Concept.objects.create(
            name="test concept",
            slug="test-concept",
            video="asfasdf",
            opening_card="aasfasdf",
        )
        self.comment = Comment.objects.create(
            user=self.main_user,
            solution=self.solution,
            comment="original comment",
        )

    def test_patch_comment_from_another_user(self):
        url = reverse(
            "solutions-api:comment-detail", kwargs={"comment_pk": self.comment.pk}
        )
        response = self.client.patch(url, {"comment": "Good amend!"})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.client.login(username="othertestuser", password="pass0209")

        url = reverse(
            "solutions-api:comment-detail", kwargs={"comment_pk": self.comment.pk}
        )

        response = self.client.patch(url, {"comment": "something else!"})
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_post_with_too_many_referenced_entities(self):
        url = reverse("solutions-api:comment-list")
        response = self.client.post(
            url,
            {
                "concept_pk": self.concept.pk,
                "solution_pk": self.solution.pk,
                "comment": "This is just a test comment!",
            },
        )
        self.assertContains(
            response, "Comment can only be created with *one* of these", status_code=400
        )
        response = self.client.post(
            url,
            {
                "concept_pk": self.concept.pk,
                "in_response_to_pk": self.comment.pk,
                "comment": "This is just a test comment!",
            },
        )
        self.assertContains(
            response, "Comment can only be created with *one* of these", status_code=400
        )
        response = self.client.post(
            url,
            {
                "solution_pk": self.solution.pk,
                "in_response_to_pk": self.comment.pk,
                "comment": "This is just a test comment!",
            },
        )
        self.assertContains(
            response,
            "Comment can only be created with *one* of these:",
            status_code=400,
        )

    def test_post_with_too_few_referenced_entities(self):
        url = reverse("solutions-api:comment-list")
        response = self.client.post(
            url,
            {
                "comment": "This is just a test comment!",
            },
        )
        self.assertContains(
            response, "Comment must be created with one of these:", status_code=400
        )


class BasicCommentAPITestCase(BaseCommentAPITestCase):
    def setUp(self):
        super().setUp()
        self.solutions = []
        self.concepts = []
        for i in range(10):
            char = chr(97 + i)
            self.solutions.append(
                Solution.objects.create(
                    name="test solution {}".format(char),
                    slug="test-solution-{}".format(char),
                    video="asfasdf{}".format(char),
                    opening_card="aasfasdf{}".format(char),
                )
            )
            self.concepts.append(
                Concept.objects.create(
                    name="test concept {}".format(char),
                    slug="test-concept-{}".format(char),
                    video="asdasdf{}".format(char),
                    opening_card="aasdasdf{}".format(char),
                )
            )

        # create several comments with each user
        for user in (self.main_user, self.other_user):
            CommenterStatus.objects.create(user=user, approved=True)
            comment_date = datetime.utcnow().replace(
                hour=0, minute=0, second=1
            ) - timedelta(days=20)
            for i in range(40):
                comment_date = comment_date + timedelta(days=1)
                solution = self.solutions[i % 10]
                c = Comment.objects.create(
                    solution=solution,
                    user=user,
                    comment="{0}th comment from {1.username}".format(i, user),
                )
                c.created = make_aware(comment_date)
                c.save()
                print("created comment", c.user.username)

    def test_get_comments_by_solution(self):
        url = reverse("solutions-api:comment-list") + "?solution_pk={}".format(
            self.solutions[0].pk
        )
        response = self.client.get(url)
        # confirm a match
        self.assertEqual(len(response.json()), 8)
        print(response.json())
        self.assertContains(response, "30th comment from maintestuser")
        self.assertContains(response, "20th comment from maintestuser")
        self.assertContains(response, "10th comment from othertestuser")
        self.assertContains(response, "0th comment from othertestuser")

    def test_get_comments_by_user(self):
        # filter for main user
        url = reverse("solutions-api:comment-list") + "?user_pk={}".format(
            self.main_user.pk
        )
        response = self.client.get(url)
        # confirm a match
        self.assertEqual(len(response.json()), 40)
        print(response.json())
        self.assertContains(response, "39th comment from maintestuser")

        # filter for other user
        url = reverse("solutions-api:comment-list") + "?user={}".format(
            self.other_user.username
        )
        response = self.client.get(url)
        # confirm a match
        self.assertEqual(len(response.json()), 40)
        self.assertContains(response, "39th comment from othertestuser")

    def test_get_comments_by_date(self):
        now = datetime.now()
        url = reverse("solutions-api:comment-list") + "?created__gt={}".format(now)
        response = self.client.get(url)
        # confirm a match
        self.assertEqual(len(response.json()), 40)
        self.assertContains(response, "39th comment from maintestuser")
        self.assertContains(response, "39th comment from othertestuser")

        # filter for other user
        url = reverse("solutions-api:comment-list") + "?created__lt={}".format(now)
        response = self.client.get(url)
        # confirm a match
        self.assertEqual(len(response.json()), 40)
        self.assertContains(response, "0th comment from maintestuser")
        self.assertContains(response, "0th comment from othertestuser")


class HypothesisCommentAPITestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.client = APIClient()
        self.main_user = get_user_model().objects.create_user(
            username="maintestuser", password="pass0209"
        )
        self.solution = Solution.objects.create(
            name="test solution",
            slug="test-solution",
            video="asfasdf",
            opening_card="aasfasdf",
        )
        self.comment = Comment.objects.create(
            user=self.main_user,
            solution=self.solution,
            comment="original comment",
        )
        self.client.login(username="maintestuser", password="pass0209")

    def test_post_comment(self):
        url = reverse("solutions-api:comment-list")

        @given(
            st.text(
                max_size=140,
                min_size=1,
                alphabet=st.characters(blacklist_categories=("Cs", "Cf", "Cc")),
            )
        )
        def _test(s):
            assume(str.strip(s) != "")
            response = self.client.post(
                url, {"solution_pk": self.solution.pk, "comment": s}
            )
            print(response.json())
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        _test()

    def test_patch_comment(self):
        # make a comment
        comment = Comment.objects.create(
            user=self.main_user,
            solution=self.solution,
            comment="original comment",
        )

        url = reverse("solutions-api:comment-detail", kwargs={"comment_pk": comment.pk})

        @given(
            st.text(
                max_size=140,
                min_size=1,
                alphabet=st.characters(blacklist_categories=("Cs", "Cf", "Cc")),
            )
        )
        def _test(s):
            assume(str.strip(s) != "")
            response = self.client.patch(url, {"comment": s})
            print(response.json())
            self.assertEqual(response.status_code, status.HTTP_200_OK)

        _test()


class UnapprovedCommentAPITestCase(APITestCase, ModelInstanceMixin):
    def setUp(self):
        self.user = self._user(username="testuser", password="pass0209")
        self._commenter_status(user=self.user, approved=False)
        self._comment(user=self.user, comment="this will need to be reviewed")
        super().setUp()

    def test_unapproved_user_sees_comment(self):
        # get comments as user
        self.client.login(username="testuser", password="pass0209")
        url = reverse("solutions-api:comment-list")
        response = self.client.get(url)
        self.assertContains(response, "this will need to be reviewed")
        self.assertTrue(response.data[0]["needs_review"])

    def test_other_user_doesnt_see_unapproved_comment(self):
        self.user = self._user(username="othertestuser", password="pass0209")
        self.client.login(username="othertestuser", password="pass0209")
        url = reverse("solutions-api:comment-list")
        response = self.client.get(url)
        print(response.data)
        self.assertNotContains(response, "this will need to be reviewed")


class BannedCommentAPITestCase(APITestCase, ModelInstanceMixin):
    def setUp(self):
        self.user = self._user(username="testuser", password="pass0209")
        self._commenter_status(user=self.user, banned=True)
        self._comment(user=self.user, comment="particularly offensive")
        super().setUp()

    def test_banned_user_sees_comment(self):
        # get comments as user
        self.client.login(username="testuser", password="pass0209")
        url = reverse("solutions-api:comment-list")
        response = self.client.get(url)
        self.assertContains(response, "particularly offensive")

    def test_other_user_doesnt_see_banned_comment(self):
        self.user = self._user(username="othertestuser", password="pass0209")
        self.client.login(username="othertestuser", password="pass0209")
        url = reverse("solutions-api:comment-list")
        response = self.client.get(url)
        print(response.data)
        self.assertNotContains(response, "particularly offensive")
