from django.db.models import Q
from django_filters import rest_framework as filters
from rest_framework.permissions import BasePermission, IsAdminUser
from rest_framework.viewsets import ModelViewSet

from solution.api.serializers import (
    CommentSerializer,
    CourseSerializer,
    StreamSerializer,
    StreamSupplementSerializer,
)
from solution.models import Comment, Course, Stream, StreamSupplement


class CourseViewSet(ModelViewSet):
    model = Course
    serializer_class = CourseSerializer
    lookup_url_kwarg = "course_slug"
    permission_classes = [IsAdminUser]
    queryset = Course.objects.all()
    lookup_field = "slug"


class StreamViewSet(ModelViewSet):
    model = Stream
    serializer_class = StreamSerializer
    lookup_url_kwarg = "stream_pk"
    permission_classes = [IsAdminUser]
    queryset = Stream.objects.all()


class StreamSupplementViewSet(ModelViewSet):
    model = StreamSupplement
    serializer_class = StreamSupplementSerializer
    lookup_url_kwarg = "stream_supplement_pk"
    permission_classes = [IsAdminUser]
    queryset = StreamSupplement.objects.all()


class CommentFilter(filters.FilterSet):
    user = filters.CharFilter(field_name="user", lookup_expr="username")
    user_pk = filters.NumberFilter(field_name="user", lookup_expr="pk")
    solution_slug = filters.CharFilter(field_name="solution", lookup_expr="slug")
    solution_pk = filters.CharFilter(field_name="solution", lookup_expr="pk")
    concept_slug = filters.CharFilter(field_name="concept", lookup_expr="slug")
    concept_pk = filters.CharFilter(field_name="concept", lookup_expr="pk")
    stream_pk = filters.CharFilter(field_name="stream", lookup_expr="pk")
    in_response_to_pk = filters.CharFilter(
        field_name="in_response_to", lookup_expr="pk"
    )

    class Meta:
        model = Comment
        fields = {
            "created": ("gt", "lt"),
        }


class IsOwnerOrAdmin(BasePermission):
    """
    Custom permission to allow access to any User who holds an active
      Household.
    """

    def has_permission(self, request, view):
        return bool(request.user and request.user.is_authenticated)

    def has_object_permission(self, request, view, obj):
        return request.user == obj.user or request.user.is_staff


class CommentViewSet(ModelViewSet):
    """
    Retrieve, create, update or destroy comments.
    """

    model = Comment
    serializer_class = CommentSerializer
    lookup_url_kwarg = "comment_pk"
    filterset_class = CommentFilter
    permission_classes = [IsOwnerOrAdmin]

    def perform_create(self, serializer):
        # print(self.request.user)
        serializer.save(user=self.request.user)

    def get_queryset(self):
        # exclude comments which needs_review and isn't by the requesting user
        # exclude comments which is_rejected and isn't by the requesting user
        user = self.request.user
        return Comment.objects.filter(in_response_to=None).exclude(
            (Q(needs_review=True) | Q(is_rejected=True)) & ~Q(user=user)
        )
