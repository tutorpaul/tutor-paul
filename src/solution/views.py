from django.contrib import messages
from django.db.models import Prefetch
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.views.generic import DetailView, ListView, RedirectView

from account.mixins import LoginRequiredMixin
from donation.mixins import DonationRequiredMixin
from solution.models import (
    Announcement,
    Concept,
    Course,
    Problem,
    Solution,
    Stream,
    TextbookEdition,
    TextbookEditionPreference,
)


class AnnouncementMixin:
    course_ident_tokens = ("course",)

    def get_context_data(self, *args, **kwargs):
        ctx = super().get_context_data(*args, **kwargs)
        # get course
        ptr = self.object
        for token in self.course_ident_tokens:
            ptr = getattr(ptr, token)
        announcements = Announcement.objects.filter(
            course=ptr, show_date__lt=timezone.now(), remove_date__gt=timezone.now()
        )
        ctx.update({"announcements": announcements})
        return ctx


class TextbookView(LoginRequiredMixin, ListView):
    template_name = "textbooks.html"
    queryset = TextbookEdition.objects.all().select_related("textbook")

    def get_context_data(self, *args, **kwargs):
        ctx = super().get_context_data(*args, **kwargs)
        # gather all the textbook editions
        object_list = ctx.pop("object_list")
        # reform like {textbook_name: {'textbook': textbook, 'editions':['edition']}, ...}
        prelim_objects = {}
        for _object in object_list:
            # accumulate all the editions by textbook
            _txt = _object.textbook
            _this = prelim_objects.setdefault(
                _txt.slug, {"textbook": _txt, "editions": []}
            )
            _this["editions"].append(_object)
        new_object_list = []
        for _, prelim in prelim_objects.items():
            # drop the textbook slug
            new_object_list.append(prelim)

        ctx.update({"object_list": new_object_list})
        return ctx


class ProblemIndexView(LoginRequiredMixin, ListView):
    template_name = "problems.html"
    slug_url_kwarg = "textbook_edition_slug"
    slug_field = "textbook_edition"

    def dispatch(self, request, *args, **kwargs):
        _selected_edition = self.kwargs.get(self.slug_url_kwarg)
        self.selected_edition = get_object_or_404(
            TextbookEdition, slug=_selected_edition
        )
        self.selected_edition.set_as_preferred_for_user(request.user)

        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        ctx = super().get_context_data(*args, **kwargs)
        ctx.update(
            {
                "textbook_edition": self.selected_edition,
            }
        )
        return ctx

    def get_queryset(self, *args, **kwargs):
        slug = self.kwargs.get(self.slug_url_kwarg)
        queryset = Problem.objects.filter(textbook_edition__slug=slug)
        return queryset


class ProblemView(LoginRequiredMixin, RedirectView):
    """Redirects to a solution based on the chapter, problem number,
    and textbook edition"""

    def get_redirect_url(self, *args, **kwargs):
        textbook_edition_slug = kwargs.get("textbook_edition_slug")
        chapter_number = kwargs.get("chapter_number")
        problem_number = kwargs.get("problem_number")
        try:
            problem = Problem.objects.get(
                textbook_edition__slug=textbook_edition_slug,
                chapter_number=chapter_number,
                problem_number=problem_number,
            )
        except Problem.DoesNotExist:
            # send them back to the textbook index
            messages.error(
                self.request,
                "Couldn't find problem {}.{} in ".format(chapter_number, problem_number)
                + "{}. Perhaps look for the problem in an ".format(
                    textbook_edition_slug
                )
                + "older edition, if you find it please leave a comment to let me know!",
            )
            print("injecting message")
            url = reverse(
                "solutions:problem_index",
                kwargs={"textbook_edition_slug": textbook_edition_slug},
            )
        else:
            url = reverse(
                "solutions:solution", kwargs={"solution_slug": problem.solution.slug}
            )
        return url


class StreamView(DonationRequiredMixin, AnnouncementMixin, DetailView):
    template_name = "stream.html"
    slug_url_kwarg = "stream_slug"
    slug_field = "slug"
    queryset = Stream.objects.all().prefetch_related("supplements")
    extra_context = {"element_type": "stream"}

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx.update({"element": ctx["object"]})
        return ctx


class StreamIndexView(LoginRequiredMixin, ListView):
    template_name = "streams.html"
    queryset = Stream.objects.all()


class StreamByCourseView(LoginRequiredMixin, ListView):
    template_name = "streams.html"
    slug_url_kwarg = "course_slug"
    slug_field = "course"

    def get_queryset(self, *args, **kwargs):
        slug = self.kwargs.get(self.slug_url_kwarg)
        self.course = get_object_or_404(Course, slug=slug)
        queryset = Stream.objects.filter(course=self.course)
        return queryset

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx.update({"course": self.course})
        return ctx


class SolutionView(LoginRequiredMixin, AnnouncementMixin, DetailView):
    template_name = "problem.html"
    slug_url_kwarg = "solution_slug"
    slug_field = "slug"
    queryset = Solution.objects.all().prefetch_related("supplements")
    extra_context = {"element_type": "solution"}

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        # next problems (based on textbook edition)
        current = ctx["object"]
        user = self.request.user
        edition = TextbookEditionPreference.objects.get(user=user).textbook_edition
        editions = TextbookEdition.objects.filter(textbook=edition.textbook)

        current_problem = Problem.objects.get(
            solution=current, textbook_edition=edition
        )
        prior = (
            Problem.objects.filter(
                chapter_number=current_problem.chapter_number,
                problem_number__lt=current_problem.problem_number,
                textbook_edition=edition,
            )
            .prefetch_related(Prefetch("solution"))
            .order_by("-problem_number")
        )
        following = Problem.objects.filter(
            chapter_number=current_problem.chapter_number,
            problem_number__gt=current_problem.problem_number,
            textbook_edition=edition,
        ).prefetch_related(Prefetch("solution"))
        ctx.update(
            {
                "prior": reversed(prior[:5]),
                "current": current_problem,
                "following": following[:5],
                "editions": editions,
            }
        )
        ctx.update({"element": ctx["object"]})
        return ctx


class ConceptView(LoginRequiredMixin, AnnouncementMixin, DetailView):
    template_name = "concept.html"
    slug_url_kwarg = "concept_slug"
    slug_field = "slug"
    queryset = Concept.objects.all()
    extra_context = {"element_type": "concept"}

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        ctx.update({"element": ctx["object"]})
        return ctx


class ConceptIndexView(LoginRequiredMixin, ListView):
    template_name = "concepts.html"
    queryset = Concept.objects.all()
