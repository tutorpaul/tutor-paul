from rest_framework import serializers

from donation.models import Donation


class DonationSerializer(serializers.ModelSerializer):
    user = serializers.StringRelatedField()

    class Meta:
        model = Donation
        fields = ("pk", "user", "amount", "created", "comment", "private")
        read_only_fields = ("pk", "user", "amount", "created")


class DonationDisplaySerializer(serializers.ModelSerializer):
    donor_name = serializers.SerializerMethodField()
    comment = serializers.SerializerMethodField()

    def get_donor_name(self, obj):
        if obj.private:
            return "private"
        return str(obj.user)

    def get_comment(self, obj):
        return obj.moderated_comment

    class Meta:
        model = Donation
        fields = ("pk", "donor_name", "amount", "created", "comment")
