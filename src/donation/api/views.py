import bleach
from django_filters import rest_framework as filters
from rest_framework import mixins, viewsets
from rest_framework.permissions import IsAdminUser, IsAuthenticated

from donation.api.serializers import (
    DonationDisplaySerializer,
    DonationSerializer,
)
from donation.models import Donation


class CreateListRetrieveViewSet(
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    viewsets.GenericViewSet,
):
    pass


class DonationFilterSet(filters.FilterSet):
    class Meta:
        model = Donation
        fields = {
            "id": ["lt", "gt", "exact"],
            "created": [
                "lt",
                "gt",
                "year",
                "month",
                "day",
                "week",
                "week_day",
                "quarter",
                "hour",
            ],
        }


class DonationViewSet(CreateListRetrieveViewSet):
    queryset = Donation.objects.filter(confirmed=True, amount__gt=0).order_by(
        "-created"
    )
    model = Donation
    serializer_class = DonationDisplaySerializer
    lookup_url_kwarg = "donation_pk"
    permission_classes = [IsAdminUser]
    permission_classes_by_action = {
        "create": [IsAuthenticated],
    }
    serializer_class_by_action = {
        "create": DonationSerializer,
    }
    filterset_class = DonationFilterSet

    def perform_create(self, serializer):
        # print(self.request.user)
        instance = serializer.save(user=self.request.user)
        instance.moderated_comment = bleach.clean(instance.comment)
        instance.save()

    def get_permissions(self):
        try:
            return [
                permission()
                for permission in self.permission_classes_by_action[self.action]
            ]
        except KeyError:
            return [permission() for permission in self.permission_classes]

    def get_serializer_class(self):
        try:
            return self.serializer_class_by_action[self.action]
        except KeyError:
            return self.serializer_class
