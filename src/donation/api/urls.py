from rest_framework import routers

from donation.api.views import DonationViewSet

# from django.urls import path


app_name = "donations-api"

router = routers.SimpleRouter()
router.register(r"", DonationViewSet, basename="donation")
urlpatterns = router.urls
