from datetime import timedelta
from unittest import mock

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.utils import timezone

from donation.models import Campaign, Donation


class DonationTestCase(TestCase):
    def setUp(self):
        # create some users
        user_model = get_user_model()
        users = []
        user_kwargs = {
            "username": "user{}",
            "email": "user{}@nowhere.com",
            "password": "pass{}",
        }
        for i in range(7):
            users.append(
                user_model.objects.create(
                    **{k: v.format(i) for k, v in user_kwargs.items()}
                )
            )
        self.users = users

    def test_donation_display(self):
        # create some donations
        # make sure they show up on the donation page
        pass

    def test_campaign_amount(self):
        # create some donations (covering a datetime range)
        now = timezone.now()
        start = now - timedelta(days=4, hours=1)
        end = now - timedelta(days=2, hours=1)
        amount = 40

        for user in self.users:
            mocked_now = mock.Mock(return_value=now)
            with mock.patch("django.utils.timezone.now", mocked_now):
                Donation.objects.create(user=user, amount=amount, confirmed=True)
            now -= timedelta(days=1)
        # create a campaign
        campaign = Campaign.objects.create(start=start, end=end, target=100)
        # ensure that the amount in the campaign is accurate
        print(
            [
                "{0.user} -- {0.amount} -- {0.created}".format(d)
                for d in Donation.objects.all()
            ]
        )
        print("{0.start} -- {0.end}".format(campaign))
        self.assertEqual(campaign.collected, 80)
