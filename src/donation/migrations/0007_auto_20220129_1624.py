# Generated by Django 3.2.8 on 2022-01-29 16:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("donation", "0006_auto_20220129_1622"),
    ]

    operations = [
        migrations.AlterField(
            model_name="donation",
            name="confirmed",
            field=models.BooleanField(default=False, null=True),
        ),
        migrations.AlterField(
            model_name="donation",
            name="private",
            field=models.BooleanField(
                default=False,
                help_text="Do not show my name alongside the donation",
                null=True,
            ),
        ),
    ]
