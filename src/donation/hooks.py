import logging

logger = logging.getLogger("paypal_ipn")

from django.conf import settings  # noqa: E402
from django.contrib.auth import get_user_model  # noqa: E402
from paypal.standard.models import ST_PP_COMPLETED  # noqa: E402

from donation.models import Donation  # noqa: E402


def donation_received(sender, **kwargs):
    ipn_obj = sender
    logger.info(f"Payment received: {ipn_obj.txn_id}")
    if ipn_obj.payment_status == ST_PP_COMPLETED:
        # WARNING !
        # Check that the receiver email is the same we previously
        # set on the `business` field. (The user could tamper with
        # that fields on the payment form before it goes to PayPal)
        if ipn_obj.receiver_email != settings.DONATION_EMAIL:
            # Not a valid payment
            logger.info(f"!! Wrong receiver email: {ipn_obj.receiver_email}")
            return
        if ipn_obj.mc_currency != "USD":
            logger.info(f"!! Wrong currency: {ipn_obj.mc_currency}")
            return
        if ipn_obj.mc_gross <= 0:
            logger.info(f"!! Bad amount: {ipn_obj.mc_gross}")
            return

        custom = getattr(ipn_obj, "custom", None)
        logger.info(f">> Donation PK: {custom}")
        donation = None
        if custom:
            # ``custom`` should be the primary key of the donation object
            try:
                donation = Donation.objects.get(pk=custom)
            except Donation.DoesNotExist:
                pass
            # log the donation
        if not donation:
            logger.info(">> Creating new Donation")
            # try to find the user who donated
            payer_email = ipn_obj.payer_email
            user_model = get_user_model()
            try:
                user = user_model.objects.get(email=payer_email)
            except user_model.DoesNotExist:
                logger.info(">>>> Unknown user, creating new")
                pwd = user_model.objects.make_random_password(length=16)
                user = user_model.objects.create_user(
                    payer_email,
                    first_name=ipn_obj.first_name,
                    last_name=ipn_obj.last_name,
                    email=payer_email,
                    password=pwd,
                )
            donation = Donation.objects.create(user=user)

        donation.amount = ipn_obj.mc_gross
        donation.transaction_id = ipn_obj.txn_id
        donation.confirmed = True
        donation.save()
        logger.info("<< Done")
