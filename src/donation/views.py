from django.views.generic import TemplateView

from donation.models import Donation


class DonationView(TemplateView):
    template_name = "donate.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        donations = Donation.objects.filter(confirmed=True).order_by("-created")[:25]
        context.update({"donations": donations})
        return context
