from django.contrib import admin

from donation.models import Campaign, Donation


@admin.register(Donation)
class DonationAdmin(admin.ModelAdmin):
    model = Donation
    list_display = ("user", "amount", "private", "confirmed")


@admin.register(Campaign)
class CampaignAdmin(admin.ModelAdmin):
    model = Campaign
    list_display = ("id", "start", "end", "target", "collected", "percent_complete")
