from django.urls import path
from django.views.generic import TemplateView

from donation.views import DonationView

app_name = "donations"

urlpatterns = [
    path(
        "boo/", TemplateView.as_view(template_name="donate_cancel.html"), name="cancel"
    ),
    path(
        "thanks/",
        TemplateView.as_view(template_name="donate_complete.html"),
        name="complete",
    ),
    path("start/", DonationView.as_view(), name="start"),
    path("cost/", TemplateView.as_view(template_name="cost.html"), name="cost"),
]
