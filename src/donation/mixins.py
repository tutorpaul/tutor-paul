from django.contrib.auth.mixins import UserPassesTestMixin
from django.contrib.auth.views import redirect_to_login


class DonationRequiredMixin(UserPassesTestMixin):
    """This should be named first in the MRO, otherwise it will have no
    effect"""

    login_url = "/donations/start/"
    permission_denied_message = (
        "A donation of at least $15 is required to access this page"
    )

    def test_func(self):
        return self.request.user.is_donor

    def handle_no_permission(self):
        return redirect_to_login(
            self.request.get_full_path(),
            login_url=self.get_login_url(),
            redirect_field_name=self.get_redirect_field_name(),
        )
