from math import floor

from django.conf import settings
from django.db import models

# https://django-paypal.readthedocs.io/en/stable/standard/ipn.html


class Donation(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, related_name="donations", on_delete=models.PROTECT
    )
    amount = models.DecimalField(max_digits=6, decimal_places=2, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    transaction_id = models.CharField(max_length=25, null=True, blank=True)
    comment = models.TextField(
        null=True,
        blank=True,
        default=None,
        help_text="Tell me why you're donating today",
    )
    moderated_comment = models.TextField(
        null=True,
        blank=True,
        default=None,
        help_text="This will be displayed to the world",
    )
    private = models.BooleanField(
        null=True, default=False, help_text="Do not show my name alongside the donation"
    )
    confirmed = models.BooleanField(null=True, default=False)


class Campaign(models.Model):
    # TODO: add constraint to ensure that campaigns do not overlap
    start = models.DateTimeField()
    end = models.DateTimeField()
    target = models.IntegerField()

    @property
    def donations(self):
        return Donation.objects.filter(
            created__lte=self.end, created__gt=self.start, confirmed=True
        )

    @property
    def collected(self):
        return self.donations.aggregate(models.Sum("amount"))["amount__sum"] or 0

    @property
    def percent_complete(self):
        return max(2, min(99, floor(self.collected / self.target * 100)))
