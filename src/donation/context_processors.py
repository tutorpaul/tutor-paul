from django.utils import timezone

from donation.models import Campaign


def current_campaign_completion(request):
    """Adds the current donation solicitation campaign completion to the
    context"""
    now = timezone.now()
    try:
        current = Campaign.objects.get(start__lt=now, end__gt=now)
    except Campaign.DoesNotExist:
        return {"campaign_percent_complete": 2}
    else:
        return {"campaign_percent_complete": current.percent_complete}
