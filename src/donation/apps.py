from django.apps import AppConfig


class DonationConfig(AppConfig):
    name = "donation"

    def ready(self):
        from paypal.standard.ipn.signals import valid_ipn_received

        from donation.hooks import donation_received

        valid_ipn_received.connect(donation_received)
