"""tutor_paul URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import include, path
from django.views.generic import TemplateView

from tutor_paul.views import ContactView, StaticFileView

urlpatterns = [
    path("", TemplateView.as_view(template_name="home.html"), name="home"),
    path("auth/", include("social_django.urls", namespace="social")),
    path("admin/", admin.site.urls),
    path("account/", include("account.urls", namespace="account")),
    path("donations/", include("donation.urls", namespace="donations")),
    path("solutions/", include("solution.urls", namespace="solutions")),
    path("contact/", ContactView.as_view(), name="contact"),
    path("api/v1/", include("tutor_paul.api.urls")),
    path("paypal/", include("paypal.standard.ipn.urls")),
    path("mailinglist/", include("mailinglist.urls", namespace="mailinglist")),
    path(
        "about/tutorpaul/",
        TemplateView.as_view(template_name="about_tutorpaul.html"),
        name="about_me",
    ),
    path(
        "robots.txt",
        StaticFileView.as_view(static_file="robots.txt"),
        name="robots_txt",
    ),
]
