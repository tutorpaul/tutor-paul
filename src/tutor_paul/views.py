from django import forms
from django.conf import settings
from django.contrib import messages
from django.core.mail import EmailMessage
from django.http import FileResponse
from django.urls import reverse
from django.views import View
from django.views.generic import FormView


class ContactForm(forms.Form):
    email = forms.EmailField(required=True)
    subject = forms.CharField(required=True, min_length=8)
    message = forms.CharField(widget=forms.Textarea(), required=True, min_length=25)


class ContactView(FormView):
    form_class = ContactForm
    template_name = "contact.html"

    def get_success_url(self):
        return reverse("home")

    def get_initial(self):
        initial = {}
        user = self.request.user
        if not user.is_anonymous:
            initial.update(
                {
                    "email": user.email,
                }
            )
        return initial

    def form_valid(self, form):
        # send the email to me!
        # no worker, send it within the request-response loop
        email = EmailMessage(
            subject="[site-contact] {}".format(form.data["subject"]),
            body=form.data["message"],
            from_email=settings.CONTACT_SENDER,
            to=(settings.CONTACT_EMAIL,),
            reply_to=(form.data["email"],),
        )
        email.send()
        messages.success(self.request, "Email sent")
        return super().form_valid(form)


class StaticFileView(View):
    static_file = "robots.txt"

    def get(self, request, *args, **kwargs):
        return FileResponse(
            open(f"tutor_paul/static/{self.static_file}", "rb"),
            filename=self.static_file.rsplit("/", maxsplit=1).pop(),
        )
