from django.conf import settings
from django.urls import reverse


def base_url(request):
    """Puts the base url for the site into the context"""
    return {
        "BASE_URL": settings.BASE_URL,
        "SITE_NAME": "TutorPaul",
        "DISCORD_LINK": settings.DISCORD_LINK,
    }


def login_url(request):
    """The url for logging in to the site."""
    login_url = "/admin/login/"
    if settings.ENV not in ("DEV", "TEST"):
        login_url = reverse("social:begin", args=["google-oauth2"])
    return {"LOGIN_URL": login_url}
