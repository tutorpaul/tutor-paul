from django.contrib.auth import get_user_model


def load_user(strategy, backend, details, *args, **kwargs):
    """Determines if a user account exists upon login attempt"""
    user_model = get_user_model()
    try:
        user = user_model.objects.get(email=details["email"])
    except user_model.DoesNotExist:
        return {"user": None}
    return {"user": user}
