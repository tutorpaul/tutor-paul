import os

from .base import *  # noqa: F403

ALLOWED_HOSTS = ["www.tutorpaul.com", "www.samfac.net"]
BASE_URL = "https://www.tutorpaul.com"

ENV = "STAGING"

SECURE_SSL_REDIRECT = True
SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")

# static files management via whitenoise
MIDDLEWARE.insert(1, "whitenoise.middleware.WhiteNoiseMiddleware")  # noqa: F405
STATICFILES_STORAGE = "whitenoise.storage.CompressedManifestStaticFilesStorage"

# set up google oauth signin
INSTALLED_APPS.append("social_django")  # noqa: F405
MIDDLEWARE.append(  # noqa: F405
    "social_django.middleware.SocialAuthExceptionMiddleware"
)
AUTHENTICATION_BACKENDS = [
    "social_core.backends.google.GoogleOAuth2",
]
TEMPLATES[0]["OPTIONS"]["context_processors"].extend(  # noqa: F405
    [
        "social_django.context_processors.backends",
        "social_django.context_processors.login_redirect",
    ]
)
SOCIAL_AUTH_LOGIN_REDIRECT_URL = "home"
LOGIN_URL = "/auth/login/google-oauth2/"
ACCOUNT_LOGIN_URL = "/auth/login/google-oauth2/"
SOCIAL_AUTH_PIPELINE = (
    "social_core.pipeline.social_auth.social_details",
    "social_core.pipeline.social_auth.social_uid",
    "social_core.pipeline.social_auth.auth_allowed",
    "social_core.pipeline.social_auth.associate_by_email",
    "social_core.pipeline.social_auth.social_user",
    "social_core.pipeline.user.get_username",
    "social_core.pipeline.user.create_user",
    "social_core.pipeline.social_auth.associate_user",
    "social_core.pipeline.social_auth.load_extra_data",
    "social_core.pipeline.user.user_details",
)

SOCIAL_AUTH_PROTECTED_USER_FIELDS = ["first_name", "last_name"]
SOCIAL_AUTH_ADMIN_USER_SEARCH_FIELDS = ["email"]
SOCIAL_AUTH_USER_FIELDS = ["first_name", "last_name", "username", "email"]


SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = os.environ.get("GOOGLE_OAUTH_CLIENT_ID")
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = os.environ.get("GOOGLE_OAUTH_CLIENT_SECRET")
SOCIAL_AUTH_GOOGLE_OAUTH2_SCOPE = ["openid", "profile"]
SOCIAL_AUTH_ASSOCIATION_SERVER_URL_LENGTH = 150
SOCIAL_AUTH_ASSOCIATION_HANDLE_LENGTH = 150
SOCIAL_AUTH_UID_LENGTH = 223

EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
MAILINGLIST_BASE_URL = BASE_URL
