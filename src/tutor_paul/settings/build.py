from .base import *  # noqa: F403

ENV = "BUILD"

# static files management via whitenoise
MIDDLEWARE.insert(1, "whitenoise.middleware.WhiteNoiseMiddleware")  # noqa: F405
STATICFILES_STORAGE = "whitenoise.storage.CompressedManifestStaticFilesStorage"

INSTALLED_APPS.append("compressor")  # noqa: F405
