from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models import Sum
from django_enumfield import enum


class TermEnum(enum.Enum):
    SPRING = 0
    SUMMER = 1
    FALL = 2

    __labels__ = {
        SPRING: "Spring",
        SUMMER: "Summer",
        FALL: "Fall",
    }


class User(AbstractUser):
    in_kind_donation = models.BooleanField(default=False)
    graduation_term = enum.EnumField(TermEnum, null=True, blank=True)
    graduation_year = models.PositiveSmallIntegerField(null=True, blank=True)

    def __str__(self):
        ret = self.username
        if self.first_name is not None and self.last_name is not None:
            ret = self.first_name + " " + self.last_name
        return ret

    def donation_total(self):
        amount = (
            self.donations.filter(confirmed=True).aggregate(amount_sum=Sum("amount"))[
                "amount_sum"
            ]
            or 0
        )
        return amount

    @property
    def is_donor(self):
        if self.in_kind_donation:
            return True
        if self.donation_total() >= 15:
            return True
        return False
