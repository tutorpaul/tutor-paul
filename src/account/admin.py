# Register your models here.
from django.contrib import admin

from account.models import User


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    model = User
    list_display = ["__str__", "donation_total", "date_joined", "last_login"]
