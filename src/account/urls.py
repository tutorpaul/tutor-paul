from django.urls import path

from account import views

app_name = "account"

urlpatterns = [
    path("logout/", views.LogoutView.as_view(), name="logout"),
    path("profile/", views.ProfileView.as_view(), name="profile"),
]
