from django.core.management.base import BaseCommand

from account.models import User

# from django.core.files import File as DjangoFile


class Command(BaseCommand):
    help = "Creates some stuff for demo/testing"

    def handle(self, *args, **options):
        master_password = "pass0209"
        User.objects.create_user(username="testuser-00", password=master_password)
        User.objects.create_user(username="testuser-01", password=master_password)
        User.objects.create_user(username="testuser-02", password=master_password)
        User.objects.create_user(username="testuser-03", password=master_password)
        User.objects.create_user(username="testuser-04", password=master_password)
