from datetime import date

from django import forms
from django.contrib import auth
from django.shortcuts import redirect
from django.views.generic import FormView
from django.views.generic.base import TemplateResponseMixin, View
from django.views.generic.edit import ModelFormMixin
from django_enumfield.forms.fields import EnumChoiceField

from account.models import TermEnum, User


class LogoutView(TemplateResponseMixin, View):
    template_name = "logout.html"

    def get(self, *args, **kwargs):
        if not self.request.user.is_authenticated:
            return redirect("/")
        return self.render_to_response({})

    def post(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            auth.logout(self.request)
        return redirect("/")


class ProfileForm(forms.ModelForm):
    graduation_term = EnumChoiceField(TermEnum)
    graduation_year = forms.IntegerField(
        max_value=date.today().year + 5, min_value=date.today().year - 1
    )

    class Meta:
        model = User
        fields = ["first_name", "last_name", "graduation_term", "graduation_year"]


class ProfileView(ModelFormMixin, FormView):
    template_name = "profile.html"
    form_class = ProfileForm
    success_url = "/"

    def dispatch(self, request, *args, **kwargs):
        self.object = self.request.user
        return super().dispatch(request, *args, **kwargs)
