from urllib.parse import urlparse, urlunparse

from django.conf import settings
from django.http import HttpResponseRedirect, QueryDict
from django.urls import NoReverseMatch, reverse


class LoginRequiredMixin(object):
    """This should be named first in the MRO, otherwise it will have no
    effect"""

    redirect_field_name = "next"
    login_url = None

    def dispatch(self, request, *args, **kwargs):
        self.request = request
        self.args = args
        self.kwargs = kwargs
        if request.user.is_authenticated:
            return super().dispatch(request, *args, **kwargs)
        return self.redirect_to_login()

    def get_login_url(self):
        return self.login_url or settings.LOGIN_URL

    def get_next_url(self):
        return self.request.get_full_path()

    def redirect_to_login(self):
        """Redirects unauthenticated requests to the login page."""
        # print('handle_redirect_to_login')
        login_url = self.get_login_url()
        redirect_field_name = self.redirect_field_name
        next_url = self.get_next_url()
        if login_url is None:
            login_url = settings.ACCOUNT_LOGIN_URL
        if next_url is None:
            next_url = self.request.get_full_path()
        try:
            login_url = reverse(login_url)
        except NoReverseMatch:
            if callable(login_url):
                raise
            if "/" not in login_url and "." not in login_url:
                raise
        url_bits = list(urlparse(login_url))
        if redirect_field_name:
            querystring = QueryDict(url_bits[4], mutable=True)
            querystring[redirect_field_name] = next_url
            url_bits[4] = querystring.urlencode(safe="/")
        # print(f'should be redirecting to {url_bits}')
        return HttpResponseRedirect(urlunparse(url_bits))
