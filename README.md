# Tutor-Paul

## Setting up a dev instance

1. Install [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/) then create and enter a virtualenv for working on this project,
2. Clone this repo and `cd` into it,
3. Install the runtime dependencies `pip install -r requirements.txt`,
4. Install the test dependencies `pip install -r requirements.text.txt`,
5. Set the minimal required environment variables: `export DJANGO_SETTINGS_MODULE=tutor_paul.settings.base` (other environment variables are required for testing certain features. If you need values for any of these please contact the maintainer),
6. Start a dev server with `python manage.py runserver` from within the `tutor_paul` directory,
7. Run tests with `pytest` (again from within the `tutor_paul` directory),
8. Do linting steps (again from within the `tutor_paul` directory) using:
   * `black .`
   * `isort -y`
   * `flake8`

## Providing SSL on the local dev

Authenticating with SSL is necessary for using Google OAuth 2, but by default Django does not provide SSL. Fortunately by installing a few libraries it will work fine. Note that these *are not* required for the production site, so they will not be added to `requirements.txt`.

```
(tutor-paul) $ python -m pip install django-extensions Werkzeug pyOpenSSL
```

Add `django-extensions` to the `INSTALLED_APPS` within settings.


Then you can start the secure server with:
```
(tutor-paul) $ python manage.py runserver_plus --cert devcert
```

Then you can access your secure server at `https://localhost:8000`.
