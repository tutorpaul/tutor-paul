FROM registry.gitlab.com/tutorpaul/tp-deps:latest-master as builder

COPY requirements.txt /wheels
COPY requirements.test.txt /wheels
WORKDIR /wheels

RUN python -m pip install -r requirements.txt -f /wheels
RUN python -m pip install -r requirements.test.txt -f /wheels

FROM python:3.11-alpine

RUN apk add --no-cache postgresql-dev libpq build-base
COPY --from=builder /wheels /wheels
WORKDIR /wheels

COPY requirements.txt /wheels
COPY requirements.test.txt /wheels
RUN python -m pip install -r requirements.txt -f /wheels
RUN python -m pip install -r requirements.test.txt -f /wheels
RUN rm -rf /wheels \
   && rm -rf /root/.cache/pip/*

# minimal environment variables for performing the compilescss and collectstatic
# Note that these will be overridden by the docker-compose that launches a given instance
ENV PYTHONUNBUFFERED 1

ENV DJANGO_SETTINGS_MODULE tutor_paul.settings.build
ENV SECRET_KEY 'y7cs44b8#e*46q@9z%b2seu8&)%(8owmp$me9$npw-+hs2ue7r'
ENV DEBUG 'True'

COPY src /app/src

WORKDIR /app/src

RUN python manage.py compilescss \
    && python manage.py collectstatic --noinput

COPY entrypoint.sh /app
RUN chmod +x /app/entrypoint.sh

# set the runtime user
RUN adduser -D worker -u 1000
USER worker

EXPOSE 8000
ENTRYPOINT ["sh", "/app/entrypoint.sh"]
